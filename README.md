## 2022_Zhang-et-al__ActaMater__data-mining-of-in-situ-TEM-experiments

This repository contains 3 folders:

(1) The first folder contains the stabilized video analyzed in the paper.

(2) The second folder contains a notebook to explain/clarify the Euler angles and transformation matrix.

(3) The third folder contains the original data and code for some plots showed in the paper.